package slack

import (
	"regexp"

	"github.com/nlopes/slack"
)

type command struct {
	r *regexp.Regexp
	f Action
}

func (c *command) match(m *slack.MessageEvent) bool {
	return c.r.MatchString(m.Msg.Text)
}

func (c *command) getParams(m *slack.MessageEvent) map[string]string {
	params := make(map[string]string)

	matches := c.r.FindStringSubmatch(m.Msg.Text)
	for i, name := range c.r.SubexpNames() {
		if name != "" {
			params[name] = matches[i]
		}
	}

	return params
}
